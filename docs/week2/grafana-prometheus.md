## Connecting to a cluster

- [kubeconfig](https://kubernetes.io/docs/tasks/access-application-cluster/configure-access-multiple-clusters/)
- [kubectl](https://kubernetes.io/docs/reference/kubectl/cheatsheet/)


Show current config:

```
kubectl config view

kubectl config get-clusters
kubectl config get-contexts


kubectl config use-context <context>
```


## Operate a cluster

- [Lens](https://k8slens.dev/)
- [k9s](https://github.com/derailed/k9s)


We are going to troubleshoot a cluster.

- [x] Troubleshooting
- [x] Logs
- [x] Resource utilization
- [x] Exec a shell in a container
- [x] Restart a Deployment
- [x] Create a port-forward
- [x] Get a shell pod in the cluster.
- [x] Scale a deployment
- [x] Describe Objects

Install a troubled Chart
```
helm upgrade --install poke-chart poke-chart -f poke-chart/trouble.values.yaml
```

!!! info
    Here's a good article on how to [troubleshoot a deployment](https://learnk8s.io/troubleshooting-deployments)

## Grafana Prometheus Stack


```
# Install the opreator.
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts

helm repo update

helm install prometheus prometheus-community/kube-prometheus-stack


# get the UI creds
kubectl get secret prometheus-grafana -o jsonpath='{.data.admin-user}'  |base64 -d
kubectl get secret prometheus-grafana -o jsonpath='{.data.admin-password}'  |base64 -d


# get access to grafana
kubectl port-forward deployment/prometheus-grafana 3000

```


here's a list of metrics we can add to panels:

- prometheus_sd_kubernetes_http_request_total
- kubelet_running_container_count
- kube_node_status_capacity_cpu_cores
- kube_node_status_capacity_memory_bytes
