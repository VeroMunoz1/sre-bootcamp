## 1. Lab1 - Deployment YAML

Store everything in the lab2/ folder

- [ ] Create a deployment called `whoami` in the namespace `labs`
- [ ] Use the `jwilder/whoami` image
- [ ] 3 Replicas
- [ ] Set the container with `8000 port`

once you have your whoami.yaml file created you can apply it using:

```bash
kubectl apply -f lab2/whoami.yaml
```

- [ ] play with the replicas by changing them to 5 and then apply it.




### Port forward ➡️

lets create a port-forward to the pod to acces it.

```bash
kubectl port-forward -n labs hello-pod 8000:80 #<- here
```

visit your browser at http://localhost:8000 a few times

To exit
++ctrl+c++

!!! Note
    you can use this to access databases, services, microservices, applications inside a cluster.

### Logs

- [ ] grab the logs of the pod store them in lab1/logs.log
