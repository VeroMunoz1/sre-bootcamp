terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
}

# Obtain data from our AWS Account.
data "aws_vpcs" "vpcs" {}

data "aws_subnets" "subnets" {
  filter {
    name   = "vpc-id"
    values = [local.vpc_id]
  }
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

# define local variables.
locals {
  vpc_id          = one(data.aws_vpcs.vpcs.ids)
  first_subnet_id = data.aws_subnets.subnets.ids.0
}

# Create our resources
resource "aws_instance" "app_server" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = var.instance_type
  subnet_id     = local.first_subnet_id
  #key_name  = aws_key_pair.deployer.key_name
  tags = {
    Name = var.instance_name
    MyName = "Marcos Cano"
  }
}


# resource "aws_key_pair" "deployer" {
#   key_name   = "deployer-key"
#   public_key = file("~/.ssh/id_rsa.pub")
# }
