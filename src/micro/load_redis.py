import redis,os


REDIS_HOST = os.getenv('REDIS_HOST', 'redis')


redisClient = redis.Redis(host=REDIS_HOST, port=6379)


ttl=10000

temp_items= {
        "id:MacBook-Pro.local": {
          "value": 1,
          "visits": 100
        },
        "id:MacBook-Pro.local2": {
          "value": 12,
          "visits": 20
        },
        "id:MacBook-Pro.local3": {
          "value": 131,
          "visits": 0
        },
      }


for k in temp_items:
  # delete the key
  print(temp_items[k])
  redisClient.hset(k, mapping=temp_items[k])
  redisClient.expire(name=k, time=ttl)

